﻿Inputs:
1) Enter Connection String from Azure
2) Enter user email
3) Enter user group (from settings)

Outputs:
1) Validation of the settings
2) Validation if user is in group
3) Shows user details

