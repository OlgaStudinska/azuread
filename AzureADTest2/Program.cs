﻿using System;
using System.Threading.Tasks;
using System.Net.Http; 
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Graph;
using Nito.AsyncEx;
using System.Reflection;
using System.Linq;

namespace AzureADTest
{
    class Program
    {
        public class AzureADConnectionSetting
        {
            public string tenantName { get; set; }
            public string tenantId { get; set; }
            public string clientId { get; set; }
            public string key { get; set; }
            public string resource { get; set; }
        }

        private const string graphResourceId = "https://graph.microsoft.com/";
        private static AzureADConnectionSetting ConnectionString;

        public static void Main(string[] args)
        {
           Console.WriteLine("Please enter Azure AD Connection Settings from Lanteria HR");
           string userInput = Console.ReadLine();
            try
            {
            var strSett = userInput.Split(";".ToArray(), StringSplitOptions.RemoveEmptyEntries).Where(s => s.Contains("=")).Select(s => s.Split("=".ToArray(), 2)).Select(x => new { key = x[0].ToLower(), value = x[1] });
            ConnectionString = new AzureADConnectionSetting();
            var sett = strSett.FirstOrDefault(s => s.key == "tenantid");
            ConnectionString.tenantId = sett != null && !string.IsNullOrEmpty(sett.value) ? sett.value : ConnectionString.tenantId;
            sett = strSett.FirstOrDefault(s => s.key == "clientid");
            ConnectionString.clientId = sett != null && !string.IsNullOrEmpty(sett.value) ? sett.value : ConnectionString.clientId;
            sett = strSett.FirstOrDefault(s => s.key == "key");
            ConnectionString.key = sett != null && !string.IsNullOrEmpty(sett.value) ? sett.value : ConnectionString.key;
            ConnectionString.resource = "https://login.microsoftonline.com/" + ConnectionString.tenantId + "/oauth2/token";

                AsyncContext.Run(() => RunAsync());
                Console.WriteLine("DONE");
            }
            catch(Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid String");
                Console.ResetColor();
                Console.ReadKey();
            }
        }
        public static async Task RunAsync()
        {
            while (true)
            {
                var token = await GetAppTokenAsync(ConnectionString.resource, graphResourceId);
                var authHelper = new AuthenticationHelper() { AccessToken = token };
                var graphClient = new GraphServiceClient(authHelper);
                string inputEmail = "";
                Console.WriteLine("Enter user email to check properties:");
                inputEmail = Console.ReadLine();
               // await WriteGroup(graphClient, inputEmail);
                await ListUser(graphClient, inputEmail);
             //   await WriteManager(graphClient, inputEmail);
                
            }
        }
        // Get Token
        private static async Task<string> GetAppTokenAsync(string authority, string azureGraphAPI)
        {
            var authenticationContext = new AuthenticationContext(authority);
            var clientCred = new ClientCredential(ConnectionString.clientId, ConnectionString.key);
            
            var authenticationResult = await authenticationContext.AcquireTokenAsync(azureGraphAPI, clientCred);
            return authenticationResult.AccessToken;
        }
        public class AuthenticationHelper : IAuthenticationProvider
        {
            public string AccessToken { get; set; }
            public Task AuthenticateRequestAsync(HttpRequestMessage request)
            {
                request.Headers.Add("Authorization", "Bearer " + AccessToken);
                return Task.FromResult(0);
            }
        }
        //Get Users 
        private static async Task ListUser(GraphServiceClient graphClient, string inputEmail)
        {
            int pagecount = 0;
            try
            {
                var pagedUsers = await graphClient.Users.Request().Select("DisplayName,GivenName,Surname,UserPrincipalName,Department,JobTitle,Mail,MemberOf,Country,City,State,MobilePhone,StreetAddress").GetAsync();
                int i = 0;
                bool UserExists = false;
                while (true)
                {
                    pagecount++;
                    foreach (var emp in pagedUsers)
                    {
                        i++;
                        if (emp.UserPrincipalName.Contains(inputEmail.ToLower()))
                        {
                            UserExists = true;
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("DisplayName:"+ emp.DisplayName);
                            Console.WriteLine("GivenName:" + emp.GivenName);
                            Console.WriteLine("Surname:" + emp.Surname);
                            Console.WriteLine("Department:" + emp.Department);
                            Console.WriteLine("JobTitle:" + emp.JobTitle);
                            Console.WriteLine("Country:" + emp.Country);
                            Console.WriteLine("City:" + emp.City);
                            Console.WriteLine("State:" + emp.State);
                            Console.WriteLine("MobilePhone:" + emp.MobilePhone);
                            Console.WriteLine("StreetAddress:" + emp.StreetAddress);
                            Console.ResetColor();
                        } 
                    }
                    if (pagedUsers.NextPageRequest != null)
                    {
                        pagedUsers = await pagedUsers.NextPageRequest.GetAsync();
                    }
                    else
                    { break; }
                }
                if (UserExists == false)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("No such user");
                    Console.ResetColor();
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Can't get data " + ex);
                Console.ResetColor();
            }
            //var graphuser = graphClient.Users["Alexander Albert"];
            //var graphphoto = graphuser.Photo;
            //var photoinfo = await graphphoto.Request().GetAsync(); // <= here the exceptions is thrown
            //var photostream = await graphphoto.Content.Request().GetAsync();
            //byte[] photobyte = new byte[photostream.Length];
            //photostream.Read(photobyte, 0, (int)photostream.Length);
            //System.IO.File.WriteAllBytes(@"c:\users\oll\desktop\user1.jpg", photobyte);
            //foreach (var data in emp.AdditionalData)
            //{
            //    if (data.Key.EndsWith("employeeID"))
            //    {
            //        string my = (string)data.Value;
            //        Console.WriteLine(my);
            //    }
            //}
        }
        private static async Task<Object> GetManager(GraphServiceClient graphClient, string inputEmail)
        {
           var Manager = new Object();
            try
            {
                Manager = await graphClient.Users[inputEmail.ToLower()].Manager.Request().GetAsync();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Can't get manager " + ex);
                Console.ResetColor();
            }
            return Manager;
        }
        private static async Task<GraphServiceGroupsCollectionPage> GetGroups(GraphServiceClient graphClient, string inputEmail)
        {
            var resultAllGroups = new GraphServiceGroupsCollectionPage();
            int pagecount = 0;
            try
            {
                var pagedGroups = await graphClient.Users[inputEmail.ToLower()].MemberOf.Request().GetAsync();
                while (true)
                {
                    pagecount++;
                    foreach (Microsoft.Graph.Group group in pagedGroups)
                    {
                        if (group.ToString().Equals("Microsoft.Graph.Group"))
                        {
                            resultAllGroups.Add(group);
                        }

                    }
                    if (pagedGroups.NextPageRequest != null)
                    {
                        pagedGroups = await pagedGroups.NextPageRequest.GetAsync();
                    }
                    else
                    { break; }
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Can't get groups " + ex);
                Console.ResetColor();
            }
            return resultAllGroups;
        }
        private static async Task WriteManager(GraphServiceClient graphClient, string inputEmail)
        {
            
            try
            {
                var GraphManager = await GetManager(graphClient,inputEmail);
                Microsoft.Graph.User Manager = (Microsoft.Graph.User)GraphManager;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Manager's DisplayName:" + Manager.DisplayName);
                Console.WriteLine("Manager's UserPrincipalName (key):" + Manager.UserPrincipalName);
                Console.ResetColor();
            }
            catch(Exception ex)
            {

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Can't get manager "+ex);
                Console.ResetColor();
            }
        }
        private static async Task WriteGroup(GraphServiceClient graphClient, string inputEmail)
        {

            try
            {
                string inputId = "";
                Console.WriteLine("Enter user group id to check if this user bolongs to this group:");
                inputId = Console.ReadLine();
                var UserGroups = await GetGroups(graphClient, inputEmail);
                Microsoft.Graph.Group Group = (from gr in UserGroups where gr.Id.Contains(inputId) select gr).FirstOrDefault();
                if (Group is null)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("User is not in this group");
                    Console.ResetColor();
                }
                else               
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("User is in this group:" + Group.Id + Group.DisplayName);
                    Console.ResetColor();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Can't get group " + ex);
                Console.ResetColor();
            }
        }
    }
}

